//
//  AddProductViewController.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/7/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {
    
    enum Segment {
        case Scan
        case Find
        case ManualEntry
    }
    
    let storyboardName = "DashboardStoryboard"
    let scanID = "CameraScanStoryboardID"
    let findID = "FindProductStoryboardID"
    let manualEntryID = "ManualEntryStoryboardID"
    
    var scanVC: CameraScanViewController?
    var findVC: FindProductTableViewController?
    var manualEntryVC: ManualEntryViewController?
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var manualEntryButton: UIButton!
    
    @IBOutlet weak var addProductContainerView: UIView!
    
    var currentSegment: Segment = .Scan {
        didSet {
            if currentSegment != oldValue {
                if currentSegment == .Scan {
                    if (scanVC != nil)  {
                        for views in addProductContainerView.subviews {
                            views.removeFromSuperview()
                        }
                        self.addProductContainerView.addSubview(scanVC!.view)
                    }
                } else if currentSegment == .Find {
                    if (findVC != nil) {
                        for views in addProductContainerView.subviews {
                            views.removeFromSuperview()
                        }
                        self.addProductContainerView.addSubview(findVC!.view)
                    }
                } else if currentSegment == .ManualEntry {
                    if (manualEntryVC != nil) {
                        for views in addProductContainerView.subviews {
                            views.removeFromSuperview()
                        }
                        self.addProductContainerView.addSubview(manualEntryVC!.view)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scanVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewControllerWithIdentifier(scanID) as? CameraScanViewController
        findVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewControllerWithIdentifier(findID) as? FindProductTableViewController
        manualEntryVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewControllerWithIdentifier(manualEntryID) as? ManualEntryViewController
        
        currentSegment = .Scan
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentButtonTapped(sender: AnyObject) {
        if sender as! UIButton == scanButton {
            currentSegment = .Scan
        } else if sender as! UIButton == findButton {
            currentSegment = .Find
        } else {
            currentSegment = .ManualEntry
        }
    }
}
