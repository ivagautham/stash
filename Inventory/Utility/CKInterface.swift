//
//  CKInterface.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/6/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit
import CloudKit

var myContainer = CKContainer(identifier: "iCloud.com.Q98Studios.Inventory")

class CKInterface: NSObject {
    
    
    typealias cloudStatusBlock = (status: CKAccountStatus, error: NSError?) -> Void
    class func isCloudCompatible(completion: cloudStatusBlock) {
        myContainer.accountStatusWithCompletionHandler(completion)
    }
    
    class func saveTempRecord() {
//        let publicDatabase = myContainer.publicCloudDatabase
//        let recordID = CKRecordID(recordName: "test")
//        var record = CKRecord(recordType: "test", recordID: recordID)
//        record.setValue("adgadg", forKey: "StringField")
//        publicDatabase.saveRecord(record, completionHandler: { record, error in
//            NSLog("Saved to cloud kit")
//            self.fetchAllRecords()
//        })
    }
    
    class func fetchAllRecords() {
        let publicDatabase = myContainer.publicCloudDatabase
        let recordID = CKRecordID(recordName: "test")
        publicDatabase.fetchRecordWithID(recordID, completionHandler:{ record, error in
            println("record : \(record)")
            println("error : \(error)")
        })
    }
    
    class func fetchAllRecordZone() {
        let publicDatabase = myContainer.publicCloudDatabase
        publicDatabase.fetchAllRecordZonesWithCompletionHandler({records, error in
            if let zoneRecords = records as? [CKRecordZone] {
                for zoneRecord in zoneRecords {
                    println("zoneRecord : \(zoneRecord.zoneID.ownerName)")
                }
            }
        })
    }
}
