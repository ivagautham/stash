//
//  UIViewController.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/7/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit
import AVFoundation

var metadataObjectType = [AVMetadataObjectTypeUPCECode,
    AVMetadataObjectTypeCode39Code,
    AVMetadataObjectTypeCode39Mod43Code,
    AVMetadataObjectTypeEAN13Code,
    AVMetadataObjectTypeEAN8Code,
    AVMetadataObjectTypeCode93Code,
    AVMetadataObjectTypeCode128Code,
    AVMetadataObjectTypePDF417Code,
    AVMetadataObjectTypeQRCode,
    AVMetadataObjectTypeAztecCode,
    AVMetadataObjectTypeInterleaved2of5Code,
    AVMetadataObjectTypeITF14Code]

extension UIViewController {
    
    // MARK: - Custom Utility Scanner methods
    /**
    Returns the product, loaylty or coupoun code for a metadata dictionary.
    
    :param: metadataObjects - Dictionary to be checked.
    
    :returns: String if the metadataObjects is valid.
    */
    func getCode(metadataObjects: [AnyObject]!) -> String {
        for data in metadataObjects {
            var metaData = data as! AVMetadataObject
            if contains(metadataObjectType, metaData.type) {
                var newData = metaData as! AVMetadataMachineReadableCodeObject
                return newData.stringValue
            }
        }
        return String("")
    }
    
    func getType(metadataObjects: [AnyObject]!) -> String {
        for data in metadataObjects {
            var metaData = data as! AVMetadataObject
            if contains(metadataObjectType, metaData.type) {
                return metaData.type
            }
        }
        return String("")
    }
    
    /**
    Checks if the given product code is valid.
    
    :param: productCode - String to be checked.
    
    :returns: Bool if the productCode is valid.
    */
    func isValidCode(productCode: String) -> Bool {
        if productCode.isEmpty || productCode == "" {
            return false
        }
        return true
    }
    
    func showAlert(text: String) {
        let alertController = UIAlertController(title:nil, message: "\(text)", preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}