//
//  OutpanInterface.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/7/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit

let outpanAPIKey = "35098e421f4a366ab73c4de36cc1ad9d"
let outpanURL = "http://www.outpan.com/api/get-product.php?"

//let finalURL = "http://www.outpan.com/api/get-product.php?barcode=0885909798957&apikey=35098e421f4a366ab73c4de36cc1ad9d"
class OutpanInterface: NSObject {

    typealias ProductDetailsBlock = (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void
    class func findProduct(code: String, productDetailsBlock: ProductDetailsBlock) {//0885909798957
       
        var requestString: String = outpanURL + "barcode=" + code
        requestString = requestString + "&apikey=" + outpanAPIKey
        
        var urlRequest : NSMutableURLRequest = NSMutableURLRequest()
        urlRequest.URL = NSURL(string: requestString)
        urlRequest.HTTPMethod = "GET"

        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue(), completionHandler: productDetailsBlock)
    }
}
