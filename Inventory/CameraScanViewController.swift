//
//  CameraScanViewController.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/7/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit
import AVFoundation

class CameraScanViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var scanPreviewLayerView: UIView!
    @IBOutlet weak var flashButton: UIButton!
    
    let SCANNER_FRAME_HORIZONTAL_SPACING: CGFloat = 30.0
    let SCANNER_FRAME_RATIO: CGFloat = 0.55
    
    let SCANNER_FRAME_WIDTH_LIMIT: CGFloat = 400.0
    
    var scannerSession: AVCaptureSession?
    var scannerCaptureDevice: AVCaptureDevice?
    var scannerDeviceInput: AVCaptureDeviceInput?
    var scannerDeviceOutput: AVCaptureMetadataOutput?
    var scannerPrevLayer: AVCaptureVideoPreviewLayer?
    var scannerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCameraBarcodeScanner()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        startBarcodeSession()
        setBarcodeScannerDelegate(true)
        
        flashButton.hidden = !flashAvailable()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopBarcodeSession()
        setBarcodeScannerDelegate(false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func flashButtonTapped(sender: AnyObject) {
        
    }
    
    // MARK: - ScanViewController private methods
    private func setBarcodeScannerDelegate(flag: Bool) {
        if flag {
            scannerDeviceOutput?.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        } else {
            scannerDeviceOutput?.setMetadataObjectsDelegate(nil, queue: dispatch_get_main_queue())
        }
    }
    
    // MARK: - AVFoundation setup methods
    
    /**
    Checks if AVFoundation is compatible/allowed for barcode scanning
    */
    func isCameraBarcodeScanningCompatible() -> Bool {
        if AVCaptureDeviceInput.deviceInputWithDevice(AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo), error: nil) != nil {
            return true
        }
        return false
    }
    
    /**
    Initiats the properties for AVFoundation's camera scanner
    */
    func initCameraBarcodeScanner() {
        deinitCameraBarcodeScanner()
        setupCaptureSession()
    }
    
    /**
    Deinit the properties for AVFoundation's camera scanner
    */
    func deinitCameraBarcodeScanner() {
        scannerSession = nil
        scannerCaptureDevice = nil
        scannerDeviceInput = nil
        scannerDeviceOutput = nil
        scannerPrevLayer = nil
        scannerView = nil
    }
    
    /**
    Creates new session for camera and defaults it to video type for machine readable code
    */
    private func setupCaptureSession() {
        //init session
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("updateScreenAutorotate"), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        scannerSession = AVCaptureSession()
        scannerCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        updateScreenAutorotate()
        if scannerPrevLayer?.connection == nil {
            setupMetadataInput()
            setupMetadataOutput()
        }
    }
    
    /**
    Creates new preview layer to scan the machine readable code
    */
    private func updatePreviewLayer() {
        if scannerPrevLayer == nil {
            scannerPrevLayer = AVCaptureVideoPreviewLayer.layerWithSession(scannerSession) as? AVCaptureVideoPreviewLayer
        }
        scannerPrevLayer?.frame = scanPreviewLayerView.frame
        scannerPrevLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        self.view.layer.addSublayer(scannerPrevLayer)
        self.view.bringSubviewToFront(scannerView)
    }
    
    /**
    Tries to create an input device to scan machine readable code
    */
    private func setupMetadataInput() {
        var error:NSError?
        scannerDeviceInput = AVCaptureDeviceInput.deviceInputWithDevice(scannerCaptureDevice, error: &error) as? AVCaptureDeviceInput
        if scannerDeviceInput != nil {
            scannerSession?.addInput(scannerDeviceInput)
        } else {
            scannerSession = nil
        }
    }
    
    /**
    Create an output device to capture machine readable code
    */
    private func setupMetadataOutput() {
        scannerDeviceOutput = AVCaptureMetadataOutput()
        scannerSession?.addOutput(scannerDeviceOutput)
        
        scannerDeviceOutput?.metadataObjectTypes = scannerDeviceOutput?.availableMetadataObjectTypes
        
        let visibleMetadataOutputRect = scannerPrevLayer?.rectForMetadataOutputRectOfInterest(scannerPrevLayer!.bounds)
        scannerDeviceOutput!.rectOfInterest = visibleMetadataOutputRect!
    }
    
    /**
    Creates new scan layer to read the machine readable code
    */
    private func updateScanLayer() {
        if scannerView == nil {
            scannerView = UIView()
            scannerView?.layer.cornerRadius = 0.0
            scannerView?.layer.borderWidth = 1.0
            scannerView?.layer.borderColor = UIColor.whiteColor().CGColor
        }
        scannerView.removeFromSuperview()
        
        scannerView?.frame = getScanLayerFrame()
        scannerView?.center = CGPointMake(scanPreviewLayerView.center.x, scanPreviewLayerView.center.y)
        
        self.view.addSubview(scannerView)
    }
    
    private func getScanLayerFrame() -> CGRect {
        let scanLayerWidth = (scanPreviewLayerView.frame.width - 2*SCANNER_FRAME_HORIZONTAL_SPACING) > SCANNER_FRAME_WIDTH_LIMIT ? SCANNER_FRAME_WIDTH_LIMIT : (scanPreviewLayerView.frame.width - 2*SCANNER_FRAME_HORIZONTAL_SPACING)
        let scanLayerHeight = scanLayerWidth * SCANNER_FRAME_RATIO
        
        return CGRectMake(0, 0, scanLayerWidth, scanLayerHeight)
    }
    
    // MARK: - AVFoundation action methods
    /**
    Checks for the state and starts an AVCaptureSession instance running.
    */
    func startBarcodeSession() {
        if scannerSession?.running == false {
            scannerSession?.startRunning()
            scannerView?.layer.borderColor = UIColor.whiteColor().CGColor
            scannerView?.layer.borderWidth = 1.0
        }
    }
    
    /**
    Checks for the state and stops an AVCaptureSession instance that is currently running.
    .   Also toggles off the torch if active
    */
    func stopBarcodeSession() {
        if scannerSession?.running == true {
            scannerSession?.stopRunning()
            scannerView?.layer.borderColor = UIColor.greenColor().CGColor
            scannerView?.layer.borderWidth = 3.0
        }
    }
    
    /**
    Sets the current mode of the receiver's torch to AVCaptureTorchModeOn/AVCaptureTorchModeOff .
    */
    func toggleFlash(){
        if scannerCaptureDevice!.hasTorch {
            if scannerCaptureDevice!.torchActive == false {
                scannerCaptureDevice!.lockForConfiguration(nil)
                scannerCaptureDevice!.torchMode = .On
                scannerCaptureDevice!.setTorchModeOnWithLevel(1.0, error: nil)
                scannerCaptureDevice!.unlockForConfiguration()
            } else {
                scannerCaptureDevice!.lockForConfiguration(nil)
                scannerCaptureDevice!.torchMode = .Off
                scannerCaptureDevice!.unlockForConfiguration()
            }
        }
    }
    
    func flashAvailable() -> Bool {
        return scannerCaptureDevice?.hasTorch ?? false
    }
    
    func cameraFlashStatus() -> AVCaptureTorchMode {
        return scannerCaptureDevice?.torchMode ?? AVCaptureTorchMode.Off
    }
    
    func updateScreenAutorotate() {
        updateScanLayer()
        updatePreviewLayer()
        if scannerPrevLayer?.connection == nil {
            setupMetadataInput()
            setupMetadataOutput()
        }
        
        let connection = scannerPrevLayer?.connection
        if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.Portrait {
            connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
        } else if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.PortraitUpsideDown {
            connection?.videoOrientation = AVCaptureVideoOrientation.PortraitUpsideDown
        }else if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.LandscapeLeft {
            connection?.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
        } else {
            connection?.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
        }
    }
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate methods
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        var barcode = getCode(metadataObjects)
        var barcodeType = getType(metadataObjects)
        if isValidCode(barcode) {
            self.stopBarcodeSession()
            
            OutpanInterface.findProduct(barcode, productDetailsBlock:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
                if (data != nil) {
                    if let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary {
                        if (jsonResult != nil) {
                            println("jsonResult : \(jsonResult)")
                        } else {
                            println("error : \(error)")
                        }
                    }
                } else {
                    println("error : \(error)")
                }
                
                self.startBarcodeSession()
            })
        }
    }
    
}
