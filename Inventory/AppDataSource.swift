//
//  AppDataSource.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/7/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import Foundation
let appDataSource = AppDataSource.sharedInstance

class AppDataSource: NSObject {

    /**
    Returns the singleton instance of the AppDataSource
    
    :returns: singleton instance of AppDataSource
    */
    class var sharedInstance : AppDataSource {
        struct Singleton {
            static let instance = AppDataSource()
        }
        
        return Singleton.instance
    }
    
}