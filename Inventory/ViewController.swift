//
//  ViewController.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/4/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit
import CloudKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        CKInterface.isCloudCompatible({ (status: CKAccountStatus, error: NSError?) -> Void in
            //CKInterface.saveTempRecord()
            CKInterface.fetchAllRecords()
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

