//
//  SearchProductTableViewCell.swift
//  Inventory
//
//  Created by Gautham V Arumugavadivelmurugan/Atlanta/IBM on 7/8/15.
//  Copyright (c) 2015 Gautham Velappan. All rights reserved.
//

import UIKit

class SearchProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productBarcodeLabel: UILabel!
    @IBOutlet weak var productAvailabilityLabel: UILabel!
    @IBOutlet weak var productQuickAddButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
